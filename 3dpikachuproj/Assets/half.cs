﻿using UnityEngine;
using System.Collections;

public class half : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 hapos = GameObject.Find("Ortho_Halfline").transform.position;
        Vector3 hasca = GameObject.Find("Ortho_Halfline").transform.localScale;
        transform.position = new Vector3(hapos.x + 600, hapos.y - 245, hapos.z + 244);
        transform.localScale = new Vector3(hasca.x * 0.5f, hasca.y, hasca.z * 0.25f);
        transform.rotation = Quaternion.Euler(-90, 0, 0);
    }
}
