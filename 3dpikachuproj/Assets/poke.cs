﻿using UnityEngine;
using System.Collections;

public class poke : MonoBehaviour {
    public Renderer rend;
	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
        rend.material.color = Color.red;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 popos = GameObject.Find("Ortho_Pokeball").transform.position;
        Vector3 posca = GameObject.Find("Ortho_Pokeball").transform.localScale;
        transform.rotation = Quaternion.Euler(-90, 0, 0);
        transform.position = new Vector3((popos.x * 0.6f + 600), 55 + (0.25f * (popos.z - 100)), popos.y +34f);
        transform.localScale = new Vector3(posca.x * 0.5f, posca.y, posca.z * 0.25f);
        
        
    }
}
