﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;

public class PlayerControlsource : MonoBehaviour
{
    public GameObject BodySourceManager;

    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;
    public Vector3[,] posbuffer = new Vector3[5, 20];
    private int time = 0, ctime = 0, unittime = 2, bcnt = 0, peoplen = 0, waittime = 0;
    public int ut2 = 8, bcnt2 = 0, fordebug=0;
    public float unitlen = 0.0f;
    private Vector3 initpos = Vector3.zero;
    private Vector3 midpos = Vector3.zero;
    private Vector3 lefthand = Vector3.zero;
    private Vector3 righthand = Vector3.zero;
    private Vector3 nowpos = Vector3.zero;
    private Vector3 kkpos = Vector3.zero;

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        peoplen = 0;
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {


                if (peoplen == 1) break;    //changable
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    posbuffer.Initialize(); time = 0; waittime = 0;
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);
                peoplen = 1;
            }
        }
        if (peoplen == 0) waittime = 0;
    }

    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        return body;
    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        Kinect.JointType j1;
        Kinect.JointType j2;
        Vector3 v1;
        Vector3 v2;
        Vector3 v3;

        if (waittime < 10)
        {
            waittime++;
            return;
        }
        if (time < unittime)
        {
            if (time < unittime / 2)
            {
                time++;
                return;
            }
            Kinect.JointType jj = Kinect.JointType.SpineBase;
            Vector3 vv = GetVector3FromJoint(body.Joints[jj]);
            vv.z = -vv.z;
            vv.x *= (-vv.z) / 10.0f;
            vv.y *= (-vv.z) / 10.0f;
            if (time == unittime / 2) midpos = vv;
            midpos = 0.5f * vv + 0.5f * midpos;

            jj = Kinect.JointType.HandLeft;
            vv = GetVector3FromJoint(body.Joints[jj]);
            vv.z = -vv.z;
            vv.x *= (-vv.z) / 10.0f;
            vv.y *= (-vv.z) / 10.0f;
            if (time == unittime / 2) lefthand = vv;
            lefthand = 0.5f * vv + 0.5f * lefthand;

            jj = Kinect.JointType.HandRight;
            vv = GetVector3FromJoint(body.Joints[jj]);
            vv.z = -vv.z;
            vv.x *= (-vv.z) / 10.0f;
            vv.y *= (-vv.z) / 10.0f;
            if (time == unittime / 2) righthand = vv;
            righthand = 0.5f * vv + 0.5f * righthand;


            jj = Kinect.JointType.SpineBase;
            vv = GetVector3FromJoint(body.Joints[jj]);
            vv.z = -vv.z;
            vv.x *= (-vv.z) / 10.0f;
            vv.y *= (-vv.z) / 10.0f;
            if (time == unittime / 2) righthand = vv;
            kkpos = 0.5f * vv + 0.5f * kkpos;

            j1 = Kinect.JointType.ShoulderLeft;
            j2 = Kinect.JointType.ShoulderRight;
            v1 = GetVector3FromJoint(body.Joints[j1]);
            v1.z = (-v1.z);
            v1.x *= (-v1.z) / 10.0f;
            v1.y *= (-v1.z) / 10.0f;
            v2 = GetVector3FromJoint(body.Joints[j2]);
            v2.z = (-v2.z);
            v2.x *= (-v2.z) / 10.0f;
            v2.y *= (-v2.z) / 10.0f;
            v3 = v2 - v1;
            unitlen = Vector3.Distance(v1, v2);

            time++;
            return;
        }
        if (time == unittime)
        {
            unitlen /= (unittime / 2);
            unitlen /= 3;
            for (int i = 0; i < unittime; i++)
            {
                posbuffer[0, i] = midpos;
                posbuffer[1, i] = lefthand;
                posbuffer[2, i] = righthand;
                posbuffer[3, i] = kkpos;
            }
            initpos = midpos;
            time++;
        }

        j1 = Kinect.JointType.ShoulderLeft;
        j2 = Kinect.JointType.ShoulderRight;
        v1 = GetVector3FromJoint(body.Joints[j1]);
        v1.z = (-v1.z);
        v1.x *= (-v1.z) / 10.0f;
        v1.y *= (-v1.z) / 10.0f;
        v2 = GetVector3FromJoint(body.Joints[j2]);
        v2.z = (-v2.z);
        v2.x *= (-v2.z) / 10.0f;
        v2.y *= (-v2.z) / 10.0f;
        v3 = v2 - v1;
        unitlen = Vector3.Distance(v1, v2);

        Vector3 preplayerpos, currplayerpos;

        preplayerpos = posbuffer[0, (bcnt + 1 + unittime) % unittime];
        Kinect.JointType jp = Kinect.JointType.SpineBase;
        Vector3 vp = GetVector3FromJoint(body.Joints[jp]);
        vp.z = -vp.z;
        vp.x *= (-vp.z) / 10.0f;
        vp.y *= (-vp.z) / 10.0f;
        currplayerpos = vp;
        posbuffer[0, bcnt] = vp;
       
        vp = currplayerpos - initpos;
        Vector3 vq = currplayerpos - preplayerpos;
        if (vq.y*10.0f < unitlen) vp.y = 0.0f;
        else vp.y = 1.0f;
        pikajumping mov = GameObject.Find("Pikachujump").GetComponent<pikajumping>();
        mov.Moving(vp,currplayerpos);


        jp = Kinect.JointType.HandLeft;
        vp = GetVector3FromJoint(body.Joints[jp]);
        vp.z = -vp.z;
        vp.x *= (-vp.z) / 10.0f;
        vp.y *= (-vp.z) / 10.0f;
        posbuffer[1, bcnt2] = vp;

        jp = Kinect.JointType.HandRight;
        vp = GetVector3FromJoint(body.Joints[jp]);
        vp.z = -vp.z;
        vp.x *= (-vp.z) / 10.0f;
        vp.y *= (-vp.z) / 10.0f;
        posbuffer[2, bcnt2] = vp;

        jp = Kinect.JointType.SpineMid;
        vp = GetVector3FromJoint(body.Joints[jp]);
        vp.z = -vp.z;
        vp.x *= (-vp.z) / 10.0f;
        vp.y *= (-vp.z) / 10.0f;
        posbuffer[3, bcnt2] = vp;

        jp = Kinect.JointType.FootRight;
        vp = GetVector3FromJoint(body.Joints[jp]);
        vp.z = -vp.z;
        vp.x *= (-vp.z) / 10.0f;
        vp.y *= (-vp.z) / 10.0f;
        if (vp.y > posbuffer[2, bcnt2].y)
        {
            time = 0;
        }

        bool kl = false,kr = false;
        pikajumping pl = GameObject.Find("Pikachujump").GetComponent<pikajumping>();
        if (pl.hitctr == 1)
        {
            jp = Kinect.JointType.ShoulderLeft;
            vp = GetVector3FromJoint(body.Joints[jp]);
            vp.z = -vp.z;
            vp.x *= (-vp.z) / 10.0f;
            vp.y *= (-vp.z) / 10.0f;
            if (vp.y < posbuffer[1, bcnt2].y) kl = true;

            jp = Kinect.JointType.ShoulderRight;
            vp = GetVector3FromJoint(body.Joints[jp]);
            vp.z = -vp.z;
            vp.x *= (-vp.z) / 10.0f;
            vp.y *= (-vp.z) / 10.0f;
            if (vp.y < posbuffer[2, bcnt2].y) kr = true;

            Vector3 vv1, vv2, vv3;
            vv1.x = 0.0f; vv1.y = 50.0f; vv1.z = 50.0f; vv1 *= 3.0f;
            vv2.x = -20.0f; vv2.y = 50.0f; vv2.z = 50.0f; vv2 *= 3.0f;
            vv3.x = 20.0f; vv3.y = 50.0f; vv3.z = 50.0f; vv3 *= 3.0f;
            if (kl==true && kr == true)
            {
                GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity = vv1;
            }
            else if (kl == true)
            {
                GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity = vv2;
            }
            else if(kr == true)
            {
                GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity = vv3;
            }
        }
        if (pl.hitctr>=1) pl.hitctr++;
        


        if (pl.hitctr > 6) pl.hitctr = 0;


        bcnt++;
        bcnt %= unittime;

        bcnt2++;
        bcnt2 %= ut2;
    }



    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}
