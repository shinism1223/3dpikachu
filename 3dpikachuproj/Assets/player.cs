﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {
    public Renderer rend;
	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
        rend.material.color = Color.blue;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 plpos = GameObject.Find("Ortho_Player").transform.position;
        Vector3 plsca = GameObject.Find("Ortho_Player").transform.localScale;
        transform.rotation = Quaternion.Euler(-90, 0, 0);
        transform.position = new Vector3((plpos.x * 0.6f + 600f), 55 + (0.25f * (plpos.z - 100)), plpos.y +34f);
        transform.localScale = new Vector3(plsca.x * 0.5f, plsca.y, plsca.z * 0.25f);
        
        
    }
}
