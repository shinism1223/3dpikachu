﻿using UnityEngine;
using System.Collections;

public class ai : MonoBehaviour {
    public Renderer rend;
	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
        rend.material.color = Color.blue;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 aipos = GameObject.Find("Ortho_AI").transform.position;
        Vector3 aisca = GameObject.Find("Ortho_AI").transform.localScale;
        transform.rotation = Quaternion.Euler(-90, 0, 0);
        transform.position = new Vector3((aipos.x * 0.6f + 600f), 55 + (0.25f * (aipos.z - 100)), aipos.y + 34f);
        transform.localScale = new Vector3(aisca.x * 0.5f, aisca.y, aisca.z * 0.25f);
        

    }
}
