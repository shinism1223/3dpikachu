﻿using UnityEngine;
using System.Collections;

public class ball : MonoBehaviour
{

    // Use this for initialization
    public Rigidbody rbody;
    public Vector3 ballvec;
    public int aiscore = 0, ai2score = 0;
    public bool gameset = false;
    void Start()
    {
        Physics.gravity = new Vector3(0, -25.0F, 0);
        rbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameset == true)
        {
            transform.position = new Vector3(-1.6f, 169.5f, 100.7f);
            rbody.velocity = new Vector3(0, -25.0F, 0);
            gameset = false;
        }
        aiscript aiF = GameObject.Find("AIPikachu").GetComponent<aiscript>();
        //rbody.AddForce(aiF.aiFtoball);
        ballvec = rbody.velocity;
        //rbody.velocity += aiF.aiFtoball;
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Cube (3)")
        {
            if (transform.position.z <= 100) { ai2score++; }
            else { aiscore++; }
            gameset = true;
        }
    }
}
