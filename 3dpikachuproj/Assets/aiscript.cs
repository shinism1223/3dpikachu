﻿using UnityEngine;
using System.Collections;

public class aiscript : MonoBehaviour
{

    public Vector3 aiFtoball;
    public Vector3 ballvector;
    public Vector3 ballpos;
    public Vector3 estiord;
    public float aiPhysical = 150f;
    public int pride = 0;
    public int pressure = 0;
    public float ftime;
    public float movex, movez;
    public int lieorjump = 0;
    public bool jumpchk = false;
    public Quaternion aa = Quaternion.identity;
    public int count = 0;
    public bool liechk = false;
    // Use this for initialization
    void Start()
    {
        aa.eulerAngles = new Vector3(90, 0, 0);
        aiFtoball = new Vector3(0f, 0f, 0f);

    }
    private bool insidestadium()
    {
        Vector3 npp = transform.position;
        if (npp.x + movex <= 80.2 && npp.x + movex >= -80.2 &&
            npp.z + movez <= 268.2 && npp.z + movez >= 123)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public Vector3 CalculateBall(Vector3 bv, Vector3 bp)
    {
        Vector3 ans;
        if (bv.y < 25f)
        {
            float falltime1 = bv.y / 25f;
            float falltime2 = (-bv.y + Mathf.Sqrt(bv.y * bv.y + 25f * (2 * bp.y - 200))) / 25f;
            ftime = falltime1 * 2 + falltime2;
            ans = new Vector3(ftime * bv.x + bp.x, 100f, ftime * bv.z + bp.z);
        }
        else
        {
            ftime = (-bv.y + Mathf.Sqrt(bv.y * bv.y + 25f * (2 * bp.y - 200))) / 25f;
            ans = new Vector3(ftime * bv.x + bp.x, 100f, ftime * bv.z + bp.z);
        }
        return ans;
    }
    // Update is called once per frame
    void Update()
    {
        pokeball ballinfo = GameObject.Find("Pokeball").GetComponent<pokeball>();
        ballvector = ballinfo.ballvec;
        ballpos = ballinfo.transform.position;
        estiord = CalculateBall(ballvector, ballpos);
        Vector3 np = transform.position;
        if (np.x >= 80.2f) np.x = 80.2f;
        if (np.x <= -80.2f) np.x = -80.2f;
        if (np.z >= 268.2f) np.z = 268.2f;
        if (np.z <= 123f) np.z = 123f;
        if (np.y <= 100f) np.y = 100f;
        if (np.y >= 200f) np.y = 200f;
        transform.position = np;
        movex = movex = movez = 0f;

        if ((estiord.x - np.x) > 0) movex = 1f;
        else movex = -1f;
        if ((estiord.z - np.z) > 0) movez = 1f;
        else movez = -1f;
        // 점프 , 눕기
        if (lieorjump == 1)
        {
            if (transform.rotation.eulerAngles.x < aa.eulerAngles.x)
            {
                transform.position += new Vector3(movex, 0, movez);
                //transform.Rotate(4, 0, 0);
            }
        }
        else if (lieorjump == 2)
        {
            // jump
            transform.position += new Vector3(0, 2, 0);
            if (transform.position.y >= 150) lieorjump = 0;
        }
        else if (aiPhysical > 1)
        {
            if (transform.position.y > 100)
            {
                transform.position += new Vector3(0, -2, 0);
            }
            else if ((ballpos.y - np.y) >= 50 && (ballpos.x - np.x) <= 30 && (ballpos.x - np.x) >= -30 && (ballpos.z - np.z) >= -30 && (ballpos.z - np.z) <= 30 && jumpchk == false)
            {
                lieorjump = 2;
                jumpchk = true;
            }
            else
            {
                if (transform.rotation.eulerAngles.x != aa.eulerAngles.y)
                {
                  //  transform.Rotate(-4, 0, 0);
                }
                if (insidestadium())
                {
                    if (((estiord.x - (np.x + movex * ftime * 60)) < 0 && movex < 0) || ((estiord.x - (np.x + movex * ftime * 60)) > 0 && movex > 0))
                    {
                        if (((estiord.z - (np.z + movez * ftime * 60)) < 0 && movez < 0) || ((estiord.z - (np.z + movez * ftime * 60)) < 0 && movez < 0))
                        {
                            lieorjump = 1;
                            liechk = true;
                        }
                    }
                    if (lieorjump == 0)
                    {
                        transform.position += new Vector3(movex, 0, movez);
                    }
                }
            }
        }


        
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Pokeball")
        {
            float directx = Random.Range(-10.0f, 10.0f);
            bool smash = false;
            jumpchk = false;
            liechk = false;
            if (lieorjump == 2)
            {
                if (ballpos.z <= 150)
                {
                    //smash
                    float poss = Random.Range(0.0f, 10.0f);
                    if (poss >= 9.0)
                    {
                        smash = true;
                        GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity += new Vector3(directx, -20f, 50f) * 2.0f;
                    }
                }
                if (!smash)
                {
                    GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity = new Vector3(directx, 0f, -100f) * 2.0f;
                }
                lieorjump = 0;
            }
            else
            {
                float poss = Random.Range(0.0f, 10.0f);
                if (poss >= 5.0)
                {
                    GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity = new Vector3(5, 70f, -150f) * 2.0f;
                }
                else
                {
                    GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity = new Vector3(5, 50f, -80f) * 2.0f;
                }
            }
        }
    }
}