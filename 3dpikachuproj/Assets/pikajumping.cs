﻿using UnityEngine;
using System.Collections;

public class pikajumping : MonoBehaviour {

    public Vector3 init,velo,height;
    public bool isjumping;
    public int hitctr;
	// Use this for initialization
	void Start () {

        //        gameObject.SetActive(false);
        init.x = 0.0f; init.y = 100.0f; init.z = 0.0f;
        gameObject.transform.position = init;
        height = velo = Vector3.zero;
        isjumping = false;
        hitctr = 0;
    }

    // Update is called once per frame
    void Update() {

        int i;
        Vector3 vl1, vl2, vl;
        Vector3 vr1, vr2, vr, v;
        float ll, kl,kr;

        return;

        if (hitctr>1){ 
            PlayerControlsource pl = GameObject.Find("PlayerControl").GetComponent<PlayerControlsource>();
            vl = Vector3.zero; kl = 0.0f; kr = 0.0f;
            for (i = pl.bcnt2; i < (pl.bcnt2+6)%8; i++)
            {
                vl1 = pl.posbuffer[1, i] - pl.posbuffer[3, i];
                vl2 = pl.posbuffer[1, (i+2)%8] - pl.posbuffer[3, (i + 2)%8];
                if (vl1.y < 0.0f || vl2.y < 0.0f) continue;
                ll = Vector3.Distance(vl1, vl2);
                if (ll*3.0f> pl.unitlen && ll>kl)
                {
                    kl = ll ;
                    vl = vl2 - vl1;
                }
            }

            vr = Vector3.zero;
            for (i = pl.bcnt2; i < (pl.bcnt2+6)%8; i++)
            {
                vr1 = pl.posbuffer[2, i] - pl.posbuffer[3, i];
                vr2 = pl.posbuffer[2, (i + 2)%8] - pl.posbuffer[3, (i + 2)%8];
                if (vr1.y < 0.0f || vr2.y < 0.0f) continue;

                ll = Vector3.Distance(vr1, vr2);
                if (ll*3.0f> pl.unitlen && ll > kr)
                {
                    kr = ll;
                    vr = vr2 - vr1;
                }
            }

            v = 0.5f * vr + 0.5f * vl;


            if (v.magnitude > 0.0f)
            {
                v.Normalize();
                
                GameObject.Find("Pokeball").GetComponent<pokeball>().rbody.velocity += v * 100.0f;
            }
            hitctr = 0;
        }
        if (hitctr >= 1)
        {
            hitctr++;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Pokeball")
        {
            if (hitctr == 0) hitctr = 1;
        }
    }

    public void Moving(Vector3 p,Vector3 q)  
    {
        Vector3 pos;
        p*= 30.0f;
        if (p.y > 0 && isjumping==false)
        {
            height = Vector3.zero;
            height.y = p.y;
            velo = Vector3.zero;
            velo.y = 18.0f;
            isjumping = true;
        }
        if (isjumping == true)
        {
            height.y += velo.y;
            p.y = height.y;
            velo.y -= 3.0f;
        }
        else p.y = 0.0f;
        pos = init + p;
        if (pos.x < -85.0f) pos.x = -85.0f;
        if (pos.x > 85.0f) pos.x = 85.0f;
        if (pos.y < 100.0f)
        {
            pos.y = 100.0f;
            isjumping = false; velo.y = 0;
        }
        if (pos.y > 180.0f) pos.y = 180.0f;
        if (pos.z < -85.0f) pos.z = -85.0f;
        if (pos.z > 85.0f) pos.z = 85.0f;
        gameObject.transform.position = pos;
    }
}
