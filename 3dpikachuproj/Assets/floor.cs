﻿using UnityEngine;
using System.Collections;

public class floor : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 flpos = GameObject.Find("Ortho_Floor").transform.position;
        Vector3 flsca = GameObject.Find("Ortho_Floor").transform.localScale;
        transform.position = new Vector3(flpos.x + 600, flpos.y - 245, flpos.z + 244);
        transform.localScale = new Vector3(flsca.x * 0.5f, flsca.y, flsca.z * 0.25f);
        transform.rotation = Quaternion.Euler(-90, 0, 0);
	}
}
